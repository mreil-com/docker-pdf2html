#!/bin/bash

if [ -z "${1}" ]; then
    echo "Specify path with PDF files to mount"
    exit 1
fi

DIR="$(cd "${1}"; pwd)"
docker run -v "${DIR}:/pdf" mreil/pdf2html
