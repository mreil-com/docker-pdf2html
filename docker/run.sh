#!/bin/bash

PDF=/pdf
echo "Looking for PDF files in directory: ${PDF}"

for FILE in $(find "${PDF}" -name "*.pdf"); do
    echo Processing ${FILE}...
    pdf2htmlEX ${FILE} $(echo "${FILE}" | sed -e "s/.pdf$/.html/")
done
